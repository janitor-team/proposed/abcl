\contentsline {subsection}{\numberline {0.0.1}Preface to the First Edition}{4}{subsection.0.0.1}%
\contentsline {subsection}{\numberline {0.0.2}Preface to the Second Edition}{4}{subsection.0.0.2}%
\contentsline {subsection}{\numberline {0.0.3}Preface to the Third Edition}{4}{subsection.0.0.3}%
\contentsline {subsection}{\numberline {0.0.4}Preface to the Fourth Edition}{5}{subsection.0.0.4}%
\contentsline {subsection}{\numberline {0.0.5}Preface to the Fifth Edition}{5}{subsection.0.0.5}%
\contentsline {subsection}{\numberline {0.0.6}Preface to the Sixth Edition}{5}{subsection.0.0.6}%
\contentsline {subsection}{\numberline {0.0.7}Preface to the Seventh Edition}{5}{subsection.0.0.7}%
\contentsline {subsection}{\numberline {0.0.8}Preface to the Eighth Edition}{5}{subsection.0.0.8}%
\contentsline {subsection}{\numberline {0.0.9}Preface to the Ninth Edition}{5}{subsection.0.0.9}%
\contentsline {subsection}{\numberline {0.0.10}Preface to the Tenth Edition}{6}{subsection.0.0.10}%
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}%
\contentsline {section}{\numberline {1.1}Conformance}{7}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}ANSI Common Lisp}{7}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Contemporary Common Lisp}{8}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}License}{8}{section.1.2}%
\contentsline {section}{\numberline {1.3}Contributors}{8}{section.1.3}%
\contentsline {chapter}{\numberline {2}Running ABCL}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}Options}{11}{section.2.1}%
\contentsline {section}{\numberline {2.2}Initialization}{12}{section.2.2}%
\contentsline {chapter}{\numberline {3}Interaction with the Hosting JVM}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Lisp to Java}{13}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Low-level Java API}{13}{subsection.3.1.1}%
\contentsline {subsubsection}{Calling Java Object Methods}{13}{section*.2}%
\contentsline {subsubsection}{Calling Java object methods: dynamic dispatch}{14}{section*.3}%
\contentsline {subsubsection}{Dynamic dispatch: Caveats}{14}{section*.4}%
\contentsline {subsubsection}{Calling Java class static methods}{15}{section*.5}%
\contentsline {subsubsection}{Parameter matching for FFI dynamic dispatch}{15}{section*.6}%
\contentsline {subsubsection}{Instantiating Java objects}{15}{section*.7}%
\contentsline {subsubsection}{Accessing Java object and class fields}{15}{section*.8}%
\contentsline {section}{\numberline {3.2}Java to Lisp}{15}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Calling Lisp from Java}{15}{subsection.3.2.1}%
\contentsline {subsubsection}{Multiple Values}{16}{section*.9}%
\contentsline {subsubsection}{Introspecting a LispObject}{17}{section*.10}%
\contentsline {paragraph}{LispObject as \texttt {boolean}}{17}{section*.11}%
\contentsline {paragraph}{LispObject as a list}{17}{section*.12}%
\contentsline {section}{\numberline {3.3}Java Scripting API (JSR-223)}{17}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Conversions}{18}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Implemented JSR-223 interfaces}{18}{subsection.3.3.2}%
\contentsline {subsubsection}{The ScriptEngine}{18}{section*.13}%
\contentsline {subsection}{\numberline {3.3.3}Start-up and configuration file}{18}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Evaluation}{19}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Compilation}{19}{subsection.3.3.5}%
\contentsline {subsection}{\numberline {3.3.6}Invocation of functions and methods}{19}{subsection.3.3.6}%
\contentsline {subsection}{\numberline {3.3.7}Implementation of Java interfaces in Lisp}{19}{subsection.3.3.7}%
\contentsline {section}{\numberline {3.4}Implementation Extension Dictionaries}{20}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}The JAVA Dictionary}{20}{subsection.3.4.1}%
\contentsline {subsubsection}{Modifying the JVM CLASSPATH}{20}{section*.14}%
\contentsline {subsubsection}{Creating a synthetic Java Class at Runtime}{20}{section*.15}%
\contentsline {paragraph}{}{21}{section*.16}%
\contentsline {paragraph}{}{21}{section*.17}%
\contentsline {paragraph}{}{21}{section*.18}%
\contentsline {paragraph}{}{21}{section*.19}%
\contentsline {paragraph}{}{21}{section*.20}%
\contentsline {paragraph}{}{21}{section*.21}%
\contentsline {paragraph}{}{21}{section*.22}%
\contentsline {paragraph}{}{21}{section*.23}%
\contentsline {paragraph}{}{21}{section*.24}%
\contentsline {paragraph}{}{21}{section*.25}%
\contentsline {paragraph}{}{21}{section*.26}%
\contentsline {paragraph}{}{22}{section*.27}%
\contentsline {paragraph}{}{22}{section*.28}%
\contentsline {paragraph}{}{22}{section*.29}%
\contentsline {paragraph}{}{22}{section*.30}%
\contentsline {paragraph}{}{22}{section*.31}%
\contentsline {paragraph}{}{22}{section*.32}%
\contentsline {paragraph}{}{22}{section*.33}%
\contentsline {paragraph}{}{22}{section*.34}%
\contentsline {paragraph}{}{22}{section*.35}%
\contentsline {paragraph}{}{22}{section*.36}%
\contentsline {paragraph}{}{22}{section*.37}%
\contentsline {paragraph}{}{22}{section*.38}%
\contentsline {paragraph}{}{22}{section*.39}%
\contentsline {paragraph}{}{22}{section*.40}%
\contentsline {paragraph}{}{23}{section*.41}%
\contentsline {paragraph}{}{23}{section*.42}%
\contentsline {paragraph}{}{23}{section*.43}%
\contentsline {paragraph}{}{23}{section*.44}%
\contentsline {paragraph}{}{23}{section*.45}%
\contentsline {paragraph}{}{23}{section*.46}%
\contentsline {paragraph}{}{23}{section*.47}%
\contentsline {paragraph}{}{23}{section*.48}%
\contentsline {paragraph}{}{23}{section*.49}%
\contentsline {paragraph}{}{23}{section*.50}%
\contentsline {paragraph}{}{23}{section*.51}%
\contentsline {paragraph}{}{23}{section*.52}%
\contentsline {paragraph}{}{23}{section*.53}%
\contentsline {paragraph}{}{23}{section*.54}%
\contentsline {paragraph}{}{24}{section*.55}%
\contentsline {paragraph}{}{24}{section*.56}%
\contentsline {paragraph}{}{24}{section*.57}%
\contentsline {paragraph}{}{24}{section*.58}%
\contentsline {paragraph}{}{24}{section*.59}%
\contentsline {paragraph}{}{24}{section*.60}%
\contentsline {paragraph}{}{24}{section*.61}%
\contentsline {paragraph}{}{25}{section*.62}%
\contentsline {paragraph}{}{25}{section*.63}%
\contentsline {paragraph}{}{25}{section*.64}%
\contentsline {paragraph}{}{25}{section*.65}%
\contentsline {paragraph}{}{25}{section*.66}%
\contentsline {paragraph}{}{25}{section*.67}%
\contentsline {paragraph}{}{25}{section*.68}%
\contentsline {paragraph}{}{25}{section*.69}%
\contentsline {paragraph}{}{25}{section*.70}%
\contentsline {paragraph}{}{25}{section*.71}%
\contentsline {paragraph}{}{25}{section*.72}%
\contentsline {paragraph}{}{26}{section*.73}%
\contentsline {paragraph}{}{26}{section*.74}%
\contentsline {paragraph}{}{26}{section*.75}%
\contentsline {paragraph}{}{26}{section*.76}%
\contentsline {paragraph}{}{26}{section*.77}%
\contentsline {paragraph}{}{26}{section*.78}%
\contentsline {paragraph}{}{26}{section*.79}%
\contentsline {paragraph}{}{27}{section*.80}%
\contentsline {paragraph}{}{27}{section*.81}%
\contentsline {paragraph}{}{27}{section*.82}%
\contentsline {paragraph}{}{27}{section*.83}%
\contentsline {paragraph}{}{27}{section*.84}%
\contentsline {paragraph}{}{27}{section*.85}%
\contentsline {paragraph}{}{27}{section*.86}%
\contentsline {paragraph}{}{27}{section*.87}%
\contentsline {paragraph}{}{27}{section*.88}%
\contentsline {paragraph}{}{27}{section*.89}%
\contentsline {paragraph}{}{27}{section*.90}%
\contentsline {paragraph}{}{27}{section*.91}%
\contentsline {paragraph}{}{28}{section*.92}%
\contentsline {subsection}{\numberline {3.4.2}The THREADS Dictionary}{29}{subsection.3.4.2}%
\contentsline {paragraph}{}{30}{section*.93}%
\contentsline {paragraph}{}{30}{section*.94}%
\contentsline {paragraph}{}{30}{section*.95}%
\contentsline {paragraph}{}{30}{section*.96}%
\contentsline {paragraph}{}{30}{section*.97}%
\contentsline {paragraph}{}{30}{section*.98}%
\contentsline {paragraph}{}{30}{section*.99}%
\contentsline {paragraph}{}{30}{section*.100}%
\contentsline {paragraph}{}{30}{section*.101}%
\contentsline {paragraph}{}{30}{section*.102}%
\contentsline {paragraph}{}{30}{section*.103}%
\contentsline {paragraph}{}{30}{section*.104}%
\contentsline {paragraph}{}{30}{section*.105}%
\contentsline {paragraph}{}{30}{section*.106}%
\contentsline {paragraph}{}{31}{section*.107}%
\contentsline {paragraph}{}{31}{section*.108}%
\contentsline {paragraph}{}{31}{section*.109}%
\contentsline {paragraph}{}{31}{section*.110}%
\contentsline {paragraph}{}{31}{section*.111}%
\contentsline {paragraph}{}{31}{section*.112}%
\contentsline {paragraph}{}{31}{section*.113}%
\contentsline {paragraph}{}{31}{section*.114}%
\contentsline {paragraph}{}{31}{section*.115}%
\contentsline {paragraph}{}{31}{section*.116}%
\contentsline {paragraph}{}{31}{section*.117}%
\contentsline {paragraph}{}{31}{section*.118}%
\contentsline {paragraph}{}{31}{section*.119}%
\contentsline {subsection}{\numberline {3.4.3}The EXTENSIONS Dictionary}{32}{subsection.3.4.3}%
\contentsline {paragraph}{}{33}{section*.120}%
\contentsline {paragraph}{}{33}{section*.121}%
\contentsline {paragraph}{}{33}{section*.122}%
\contentsline {paragraph}{}{33}{section*.123}%
\contentsline {paragraph}{}{33}{section*.124}%
\contentsline {paragraph}{}{33}{section*.125}%
\contentsline {paragraph}{}{33}{section*.126}%
\contentsline {paragraph}{}{33}{section*.127}%
\contentsline {paragraph}{}{33}{section*.128}%
\contentsline {paragraph}{}{33}{section*.129}%
\contentsline {paragraph}{}{33}{section*.130}%
\contentsline {paragraph}{}{33}{section*.131}%
\contentsline {paragraph}{}{33}{section*.132}%
\contentsline {paragraph}{}{33}{section*.133}%
\contentsline {paragraph}{}{33}{section*.134}%
\contentsline {paragraph}{}{33}{section*.135}%
\contentsline {paragraph}{}{34}{section*.136}%
\contentsline {paragraph}{}{34}{section*.137}%
\contentsline {paragraph}{}{34}{section*.138}%
\contentsline {paragraph}{}{34}{section*.139}%
\contentsline {paragraph}{}{34}{section*.140}%
\contentsline {paragraph}{}{34}{section*.141}%
\contentsline {paragraph}{}{34}{section*.142}%
\contentsline {paragraph}{}{34}{section*.143}%
\contentsline {paragraph}{}{34}{section*.144}%
\contentsline {paragraph}{}{34}{section*.145}%
\contentsline {paragraph}{}{34}{section*.146}%
\contentsline {paragraph}{}{34}{section*.147}%
\contentsline {paragraph}{}{34}{section*.148}%
\contentsline {paragraph}{}{34}{section*.149}%
\contentsline {paragraph}{}{34}{section*.150}%
\contentsline {paragraph}{}{35}{section*.151}%
\contentsline {paragraph}{}{35}{section*.152}%
\contentsline {paragraph}{}{35}{section*.153}%
\contentsline {paragraph}{}{35}{section*.154}%
\contentsline {paragraph}{}{35}{section*.155}%
\contentsline {paragraph}{}{35}{section*.156}%
\contentsline {paragraph}{}{35}{section*.157}%
\contentsline {paragraph}{}{35}{section*.158}%
\contentsline {paragraph}{}{35}{section*.159}%
\contentsline {paragraph}{}{35}{section*.160}%
\contentsline {paragraph}{}{35}{section*.161}%
\contentsline {paragraph}{}{36}{section*.162}%
\contentsline {paragraph}{}{36}{section*.163}%
\contentsline {paragraph}{}{36}{section*.164}%
\contentsline {paragraph}{}{36}{section*.165}%
\contentsline {paragraph}{}{36}{section*.166}%
\contentsline {paragraph}{}{36}{section*.167}%
\contentsline {paragraph}{}{36}{section*.168}%
\contentsline {paragraph}{}{36}{section*.169}%
\contentsline {paragraph}{}{36}{section*.170}%
\contentsline {paragraph}{}{36}{section*.171}%
\contentsline {paragraph}{}{36}{section*.172}%
\contentsline {paragraph}{}{36}{section*.173}%
\contentsline {paragraph}{}{36}{section*.174}%
\contentsline {paragraph}{}{36}{section*.175}%
\contentsline {paragraph}{}{37}{section*.176}%
\contentsline {paragraph}{}{37}{section*.177}%
\contentsline {paragraph}{}{37}{section*.178}%
\contentsline {paragraph}{}{37}{section*.179}%
\contentsline {paragraph}{}{37}{section*.180}%
\contentsline {paragraph}{}{37}{section*.181}%
\contentsline {paragraph}{}{37}{section*.182}%
\contentsline {paragraph}{}{37}{section*.183}%
\contentsline {paragraph}{}{37}{section*.184}%
\contentsline {paragraph}{}{37}{section*.185}%
\contentsline {paragraph}{}{37}{section*.186}%
\contentsline {paragraph}{}{37}{section*.187}%
\contentsline {paragraph}{}{37}{section*.188}%
\contentsline {paragraph}{}{37}{section*.189}%
\contentsline {paragraph}{}{37}{section*.190}%
\contentsline {paragraph}{}{37}{section*.191}%
\contentsline {paragraph}{}{38}{section*.192}%
\contentsline {paragraph}{}{38}{section*.193}%
\contentsline {paragraph}{}{38}{section*.194}%
\contentsline {paragraph}{}{38}{section*.195}%
\contentsline {paragraph}{}{38}{section*.196}%
\contentsline {paragraph}{}{38}{section*.197}%
\contentsline {paragraph}{}{38}{section*.198}%
\contentsline {paragraph}{}{38}{section*.199}%
\contentsline {paragraph}{}{38}{section*.200}%
\contentsline {paragraph}{}{38}{section*.201}%
\contentsline {paragraph}{}{38}{section*.202}%
\contentsline {paragraph}{}{38}{section*.203}%
\contentsline {paragraph}{}{38}{section*.204}%
\contentsline {paragraph}{}{38}{section*.205}%
\contentsline {paragraph}{}{38}{section*.206}%
\contentsline {paragraph}{}{39}{section*.207}%
\contentsline {paragraph}{}{39}{section*.208}%
\contentsline {paragraph}{}{39}{section*.209}%
\contentsline {paragraph}{}{39}{section*.210}%
\contentsline {paragraph}{}{39}{section*.211}%
\contentsline {paragraph}{}{39}{section*.212}%
\contentsline {paragraph}{}{39}{section*.213}%
\contentsline {paragraph}{}{39}{section*.214}%
\contentsline {paragraph}{}{39}{section*.215}%
\contentsline {paragraph}{}{39}{section*.216}%
\contentsline {paragraph}{}{39}{section*.217}%
\contentsline {paragraph}{}{39}{section*.218}%
\contentsline {paragraph}{}{39}{section*.219}%
\contentsline {paragraph}{}{39}{section*.220}%
\contentsline {paragraph}{}{39}{section*.221}%
\contentsline {paragraph}{}{39}{section*.222}%
\contentsline {paragraph}{}{40}{section*.223}%
\contentsline {paragraph}{}{40}{section*.224}%
\contentsline {paragraph}{}{40}{section*.225}%
\contentsline {paragraph}{}{40}{section*.226}%
\contentsline {paragraph}{}{40}{section*.227}%
\contentsline {paragraph}{}{40}{section*.228}%
\contentsline {paragraph}{}{40}{section*.229}%
\contentsline {paragraph}{}{40}{section*.230}%
\contentsline {paragraph}{}{40}{section*.231}%
\contentsline {paragraph}{}{40}{section*.232}%
\contentsline {paragraph}{}{40}{section*.233}%
\contentsline {paragraph}{}{40}{section*.234}%
\contentsline {paragraph}{}{40}{section*.235}%
\contentsline {paragraph}{}{40}{section*.236}%
\contentsline {paragraph}{}{40}{section*.237}%
\contentsline {paragraph}{}{40}{section*.238}%
\contentsline {paragraph}{}{41}{section*.239}%
\contentsline {paragraph}{}{41}{section*.240}%
\contentsline {paragraph}{}{41}{section*.241}%
\contentsline {paragraph}{}{41}{section*.242}%
\contentsline {paragraph}{}{41}{section*.243}%
\contentsline {chapter}{\numberline {4}Beyond ANSI}{43}{chapter.4}%
\contentsline {section}{\numberline {4.1}Compiler to Java Virtual Machine Bytecode}{43}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Compiler Diagnostics}{43}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Decompilation}{43}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Pathname}{43}{section.4.2}%
\contentsline {subsubsection}{URL-PATHNAME}{44}{section*.244}%
\contentsline {subsubsection}{JAR-PATHNAME}{45}{section*.245}%
\contentsline {section}{\numberline {4.3}Package-Local Nicknames}{45}{section.4.3}%
\contentsline {section}{\numberline {4.4}Extensible Sequences}{47}{section.4.4}%
\contentsline {section}{\numberline {4.5}Extensions to CLOS}{47}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Metaobject Protocol}{47}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}Specializing on Java classes}{47}{subsection.4.5.2}%
\contentsline {section}{\numberline {4.6}Extensions to the Reader}{48}{section.4.6}%
\contentsline {section}{\numberline {4.7}Overloading of the CL:REQUIRE Mechanism}{48}{section.4.7}%
\contentsline {section}{\numberline {4.8}JSS extension of the Reader by SHARPSIGN-DOUBLE-QUOTE}{49}{section.4.8}%
\contentsline {section}{\numberline {4.9}ASDF}{49}{section.4.9}%
\contentsline {section}{\numberline {4.10}Extension to CL:MAKE-ARRAY}{49}{section.4.10}%
\contentsline {chapter}{\numberline {5}Contrib}{51}{chapter.5}%
\contentsline {section}{\numberline {5.1}abcl-asdf}{51}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Referencing Maven Artifacts via ASDF}{51}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}API}{51}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Directly Instructing Maven to Download JVM Artifacts}{52}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}asdf-jar}{52}{section.5.2}%
\contentsline {section}{\numberline {5.3}jss}{52}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}JSS usage}{53}{subsection.5.3.1}%
\contentsline {section}{\numberline {5.4}jfli}{53}{section.5.4}%
\contentsline {section}{\numberline {5.5}abcl-introspect}{53}{section.5.5}%
\contentsline {subsection}{\numberline {5.5.1}Implementations for CL:DISASSEMBLE}{53}{subsection.5.5.1}%
\contentsline {section}{\numberline {5.6}abcl-build}{55}{section.5.6}%
\contentsline {subsection}{\numberline {5.6.1}ABCL-BUILD Utilities}{55}{subsection.5.6.1}%
\contentsline {section}{\numberline {5.7}named-readtables}{55}{section.5.7}%
\contentsline {chapter}{\numberline {6}History}{57}{chapter.6}%
\contentsline {chapter}{\numberline {A}The MOP Dictionary}{59}{appendix.A}%
\contentsline {paragraph}{}{60}{section*.246}%
\contentsline {paragraph}{}{60}{section*.247}%
\contentsline {paragraph}{}{60}{section*.248}%
\contentsline {paragraph}{}{60}{section*.249}%
\contentsline {paragraph}{}{60}{section*.250}%
\contentsline {paragraph}{}{60}{section*.251}%
\contentsline {paragraph}{}{60}{section*.252}%
\contentsline {paragraph}{}{60}{section*.253}%
\contentsline {paragraph}{}{60}{section*.254}%
\contentsline {paragraph}{}{60}{section*.255}%
\contentsline {paragraph}{}{60}{section*.256}%
\contentsline {paragraph}{}{60}{section*.257}%
\contentsline {paragraph}{}{60}{section*.258}%
\contentsline {paragraph}{}{60}{section*.259}%
\contentsline {paragraph}{}{60}{section*.260}%
\contentsline {paragraph}{}{60}{section*.261}%
\contentsline {paragraph}{}{61}{section*.262}%
\contentsline {paragraph}{}{61}{section*.263}%
\contentsline {paragraph}{}{61}{section*.264}%
\contentsline {paragraph}{}{61}{section*.265}%
\contentsline {paragraph}{}{61}{section*.266}%
\contentsline {paragraph}{}{61}{section*.267}%
\contentsline {paragraph}{}{61}{section*.268}%
\contentsline {paragraph}{}{61}{section*.269}%
\contentsline {paragraph}{}{61}{section*.270}%
\contentsline {paragraph}{}{61}{section*.271}%
\contentsline {paragraph}{}{61}{section*.272}%
\contentsline {paragraph}{}{61}{section*.273}%
\contentsline {paragraph}{}{61}{section*.274}%
\contentsline {paragraph}{}{61}{section*.275}%
\contentsline {paragraph}{}{61}{section*.276}%
\contentsline {paragraph}{}{61}{section*.277}%
\contentsline {paragraph}{}{62}{section*.278}%
\contentsline {paragraph}{}{62}{section*.279}%
\contentsline {paragraph}{}{62}{section*.280}%
\contentsline {paragraph}{}{62}{section*.281}%
\contentsline {paragraph}{}{62}{section*.282}%
\contentsline {paragraph}{}{62}{section*.283}%
\contentsline {paragraph}{}{62}{section*.284}%
\contentsline {paragraph}{}{62}{section*.285}%
\contentsline {paragraph}{}{62}{section*.286}%
\contentsline {paragraph}{}{62}{section*.287}%
\contentsline {paragraph}{}{62}{section*.288}%
\contentsline {paragraph}{}{62}{section*.289}%
\contentsline {paragraph}{}{62}{section*.290}%
\contentsline {paragraph}{}{62}{section*.291}%
\contentsline {paragraph}{}{62}{section*.292}%
\contentsline {paragraph}{}{62}{section*.293}%
\contentsline {paragraph}{}{63}{section*.294}%
\contentsline {paragraph}{}{63}{section*.295}%
\contentsline {paragraph}{}{63}{section*.296}%
\contentsline {paragraph}{}{63}{section*.297}%
\contentsline {paragraph}{}{63}{section*.298}%
\contentsline {paragraph}{}{63}{section*.299}%
\contentsline {paragraph}{}{63}{section*.300}%
\contentsline {paragraph}{}{63}{section*.301}%
\contentsline {paragraph}{}{63}{section*.302}%
\contentsline {paragraph}{}{63}{section*.303}%
\contentsline {paragraph}{}{63}{section*.304}%
\contentsline {paragraph}{}{63}{section*.305}%
\contentsline {paragraph}{}{63}{section*.306}%
\contentsline {paragraph}{}{63}{section*.307}%
\contentsline {paragraph}{}{63}{section*.308}%
\contentsline {paragraph}{}{63}{section*.309}%
\contentsline {paragraph}{}{64}{section*.310}%
\contentsline {paragraph}{}{64}{section*.311}%
\contentsline {paragraph}{}{64}{section*.312}%
\contentsline {paragraph}{}{64}{section*.313}%
\contentsline {paragraph}{}{64}{section*.314}%
\contentsline {paragraph}{}{64}{section*.315}%
\contentsline {paragraph}{}{64}{section*.316}%
\contentsline {paragraph}{}{64}{section*.317}%
\contentsline {paragraph}{}{64}{section*.318}%
\contentsline {paragraph}{}{64}{section*.319}%
\contentsline {paragraph}{}{64}{section*.320}%
\contentsline {paragraph}{}{64}{section*.321}%
\contentsline {paragraph}{}{64}{section*.322}%
\contentsline {paragraph}{}{64}{section*.323}%
\contentsline {paragraph}{}{64}{section*.324}%
\contentsline {paragraph}{}{64}{section*.325}%
\contentsline {paragraph}{}{65}{section*.326}%
\contentsline {paragraph}{}{65}{section*.327}%
\contentsline {paragraph}{}{65}{section*.328}%
\contentsline {paragraph}{}{65}{section*.329}%
\contentsline {paragraph}{}{65}{section*.330}%
\contentsline {paragraph}{}{65}{section*.331}%
\contentsline {paragraph}{}{65}{section*.332}%
\contentsline {paragraph}{}{65}{section*.333}%
\contentsline {paragraph}{}{65}{section*.334}%
\contentsline {paragraph}{}{65}{section*.335}%
\contentsline {paragraph}{}{65}{section*.336}%
\contentsline {chapter}{\numberline {B}The SYSTEM Dictionary}{67}{appendix.B}%
\contentsline {paragraph}{}{68}{section*.337}%
\contentsline {paragraph}{}{68}{section*.338}%
\contentsline {paragraph}{}{68}{section*.339}%
\contentsline {paragraph}{}{68}{section*.340}%
\contentsline {paragraph}{}{68}{section*.341}%
\contentsline {paragraph}{}{68}{section*.342}%
\contentsline {paragraph}{}{68}{section*.343}%
\contentsline {paragraph}{}{68}{section*.344}%
\contentsline {paragraph}{}{68}{section*.345}%
\contentsline {paragraph}{}{68}{section*.346}%
\contentsline {paragraph}{}{68}{section*.347}%
\contentsline {paragraph}{}{68}{section*.348}%
\contentsline {paragraph}{}{68}{section*.349}%
\contentsline {paragraph}{}{68}{section*.350}%
\contentsline {paragraph}{}{68}{section*.351}%
\contentsline {paragraph}{}{68}{section*.352}%
\contentsline {paragraph}{}{69}{section*.353}%
\contentsline {paragraph}{}{69}{section*.354}%
\contentsline {paragraph}{}{69}{section*.355}%
\contentsline {paragraph}{}{69}{section*.356}%
\contentsline {paragraph}{}{69}{section*.357}%
\contentsline {paragraph}{}{69}{section*.358}%
\contentsline {paragraph}{}{69}{section*.359}%
\contentsline {paragraph}{}{69}{section*.360}%
\contentsline {paragraph}{}{69}{section*.361}%
\contentsline {paragraph}{}{69}{section*.362}%
\contentsline {paragraph}{}{69}{section*.363}%
\contentsline {paragraph}{}{69}{section*.364}%
\contentsline {paragraph}{}{69}{section*.365}%
\contentsline {paragraph}{}{69}{section*.366}%
\contentsline {paragraph}{}{69}{section*.367}%
\contentsline {paragraph}{}{69}{section*.368}%
\contentsline {paragraph}{}{70}{section*.369}%
\contentsline {paragraph}{}{70}{section*.370}%
\contentsline {paragraph}{}{70}{section*.371}%
\contentsline {paragraph}{}{70}{section*.372}%
\contentsline {paragraph}{}{70}{section*.373}%
\contentsline {paragraph}{}{70}{section*.374}%
\contentsline {paragraph}{}{70}{section*.375}%
\contentsline {paragraph}{}{70}{section*.376}%
\contentsline {paragraph}{}{70}{section*.377}%
\contentsline {paragraph}{}{70}{section*.378}%
\contentsline {paragraph}{}{70}{section*.379}%
\contentsline {paragraph}{}{70}{section*.380}%
\contentsline {paragraph}{}{70}{section*.381}%
\contentsline {paragraph}{}{70}{section*.382}%
\contentsline {paragraph}{}{70}{section*.383}%
\contentsline {paragraph}{}{70}{section*.384}%
\contentsline {paragraph}{}{71}{section*.385}%
\contentsline {paragraph}{}{71}{section*.386}%
\contentsline {paragraph}{}{71}{section*.387}%
\contentsline {paragraph}{}{71}{section*.388}%
\contentsline {paragraph}{}{71}{section*.389}%
\contentsline {paragraph}{}{71}{section*.390}%
\contentsline {paragraph}{}{71}{section*.391}%
\contentsline {paragraph}{}{71}{section*.392}%
\contentsline {paragraph}{}{71}{section*.393}%
\contentsline {paragraph}{}{71}{section*.394}%
\contentsline {paragraph}{}{71}{section*.395}%
\contentsline {paragraph}{}{71}{section*.396}%
\contentsline {paragraph}{}{71}{section*.397}%
\contentsline {paragraph}{}{71}{section*.398}%
\contentsline {paragraph}{}{71}{section*.399}%
\contentsline {paragraph}{}{71}{section*.400}%
\contentsline {paragraph}{}{72}{section*.401}%
\contentsline {paragraph}{}{72}{section*.402}%
\contentsline {paragraph}{}{72}{section*.403}%
\contentsline {paragraph}{}{72}{section*.404}%
\contentsline {paragraph}{}{72}{section*.405}%
\contentsline {paragraph}{}{72}{section*.406}%
\contentsline {paragraph}{}{72}{section*.407}%
\contentsline {paragraph}{}{72}{section*.408}%
\contentsline {paragraph}{}{72}{section*.409}%
\contentsline {paragraph}{}{72}{section*.410}%
\contentsline {paragraph}{}{72}{section*.411}%
\contentsline {paragraph}{}{72}{section*.412}%
\contentsline {paragraph}{}{72}{section*.413}%
\contentsline {paragraph}{}{72}{section*.414}%
\contentsline {paragraph}{}{72}{section*.415}%
\contentsline {paragraph}{}{73}{section*.416}%
\contentsline {paragraph}{}{73}{section*.417}%
\contentsline {paragraph}{}{73}{section*.418}%
\contentsline {paragraph}{}{73}{section*.419}%
\contentsline {paragraph}{}{73}{section*.420}%
\contentsline {paragraph}{}{73}{section*.421}%
\contentsline {paragraph}{}{73}{section*.422}%
\contentsline {paragraph}{}{73}{section*.423}%
\contentsline {paragraph}{}{73}{section*.424}%
\contentsline {paragraph}{}{73}{section*.425}%
\contentsline {paragraph}{}{73}{section*.426}%
\contentsline {paragraph}{}{73}{section*.427}%
\contentsline {paragraph}{}{73}{section*.428}%
\contentsline {paragraph}{}{73}{section*.429}%
\contentsline {paragraph}{}{74}{section*.430}%
\contentsline {paragraph}{}{74}{section*.431}%
\contentsline {paragraph}{}{74}{section*.432}%
\contentsline {paragraph}{}{74}{section*.433}%
\contentsline {paragraph}{}{74}{section*.434}%
\contentsline {paragraph}{}{74}{section*.435}%
\contentsline {paragraph}{}{74}{section*.436}%
\contentsline {paragraph}{}{74}{section*.437}%
\contentsline {paragraph}{}{74}{section*.438}%
\contentsline {paragraph}{}{74}{section*.439}%
\contentsline {paragraph}{}{74}{section*.440}%
\contentsline {paragraph}{}{74}{section*.441}%
\contentsline {paragraph}{}{74}{section*.442}%
\contentsline {paragraph}{}{74}{section*.443}%
\contentsline {paragraph}{}{74}{section*.444}%
\contentsline {paragraph}{}{74}{section*.445}%
\contentsline {paragraph}{}{75}{section*.446}%
\contentsline {paragraph}{}{75}{section*.447}%
\contentsline {paragraph}{}{75}{section*.448}%
\contentsline {paragraph}{}{75}{section*.449}%
\contentsline {paragraph}{}{75}{section*.450}%
\contentsline {paragraph}{}{75}{section*.451}%
\contentsline {paragraph}{}{75}{section*.452}%
\contentsline {paragraph}{}{75}{section*.453}%
\contentsline {paragraph}{}{75}{section*.454}%
\contentsline {paragraph}{}{75}{section*.455}%
\contentsline {paragraph}{}{75}{section*.456}%
\contentsline {paragraph}{}{75}{section*.457}%
\contentsline {paragraph}{}{75}{section*.458}%
\contentsline {paragraph}{}{75}{section*.459}%
\contentsline {paragraph}{}{76}{section*.460}%
\contentsline {paragraph}{}{76}{section*.461}%
\contentsline {paragraph}{}{76}{section*.462}%
\contentsline {paragraph}{}{76}{section*.463}%
\contentsline {paragraph}{}{76}{section*.464}%
\contentsline {paragraph}{}{76}{section*.465}%
\contentsline {paragraph}{}{76}{section*.466}%
\contentsline {paragraph}{}{76}{section*.467}%
\contentsline {paragraph}{}{76}{section*.468}%
\contentsline {paragraph}{}{76}{section*.469}%
\contentsline {paragraph}{}{76}{section*.470}%
\contentsline {paragraph}{}{76}{section*.471}%
\contentsline {paragraph}{}{76}{section*.472}%
\contentsline {paragraph}{}{76}{section*.473}%
\contentsline {paragraph}{}{76}{section*.474}%
\contentsline {paragraph}{}{76}{section*.475}%
\contentsline {paragraph}{}{77}{section*.476}%
\contentsline {paragraph}{}{77}{section*.477}%
\contentsline {paragraph}{}{77}{section*.478}%
\contentsline {paragraph}{}{77}{section*.479}%
\contentsline {paragraph}{}{77}{section*.480}%
\contentsline {paragraph}{}{77}{section*.481}%
\contentsline {paragraph}{}{77}{section*.482}%
\contentsline {paragraph}{}{77}{section*.483}%
\contentsline {paragraph}{}{77}{section*.484}%
\contentsline {paragraph}{}{77}{section*.485}%
\contentsline {paragraph}{}{77}{section*.486}%
\contentsline {paragraph}{}{77}{section*.487}%
\contentsline {paragraph}{}{77}{section*.488}%
\contentsline {paragraph}{}{77}{section*.489}%
\contentsline {paragraph}{}{77}{section*.490}%
\contentsline {paragraph}{}{78}{section*.491}%
\contentsline {paragraph}{}{78}{section*.492}%
\contentsline {paragraph}{}{78}{section*.493}%
\contentsline {paragraph}{}{78}{section*.494}%
\contentsline {paragraph}{}{78}{section*.495}%
\contentsline {paragraph}{}{78}{section*.496}%
\contentsline {paragraph}{}{78}{section*.497}%
\contentsline {paragraph}{}{78}{section*.498}%
\contentsline {paragraph}{}{78}{section*.499}%
\contentsline {paragraph}{}{78}{section*.500}%
\contentsline {paragraph}{}{78}{section*.501}%
\contentsline {paragraph}{}{78}{section*.502}%
\contentsline {paragraph}{}{78}{section*.503}%
\contentsline {paragraph}{}{78}{section*.504}%
\contentsline {paragraph}{}{78}{section*.505}%
\contentsline {paragraph}{}{78}{section*.506}%
\contentsline {paragraph}{}{79}{section*.507}%
\contentsline {paragraph}{}{79}{section*.508}%
\contentsline {paragraph}{}{79}{section*.509}%
\contentsline {paragraph}{}{79}{section*.510}%
\contentsline {paragraph}{}{79}{section*.511}%
\contentsline {paragraph}{}{79}{section*.512}%
\contentsline {paragraph}{}{79}{section*.513}%
\contentsline {paragraph}{}{79}{section*.514}%
\contentsline {paragraph}{}{79}{section*.515}%
\contentsline {paragraph}{}{79}{section*.516}%
\contentsline {paragraph}{}{79}{section*.517}%
\contentsline {paragraph}{}{79}{section*.518}%
\contentsline {paragraph}{}{79}{section*.519}%
\contentsline {paragraph}{}{79}{section*.520}%
\contentsline {paragraph}{}{79}{section*.521}%
\contentsline {paragraph}{}{79}{section*.522}%
\contentsline {paragraph}{}{80}{section*.523}%
\contentsline {paragraph}{}{80}{section*.524}%
\contentsline {paragraph}{}{80}{section*.525}%
\contentsline {paragraph}{}{80}{section*.526}%
\contentsline {paragraph}{}{80}{section*.527}%
\contentsline {paragraph}{}{80}{section*.528}%
\contentsline {paragraph}{}{80}{section*.529}%
\contentsline {paragraph}{}{80}{section*.530}%
\contentsline {paragraph}{}{80}{section*.531}%
\contentsline {paragraph}{}{80}{section*.532}%
\contentsline {paragraph}{}{80}{section*.533}%
\contentsline {paragraph}{}{80}{section*.534}%
\contentsline {paragraph}{}{80}{section*.535}%
\contentsline {paragraph}{}{80}{section*.536}%
\contentsline {paragraph}{}{80}{section*.537}%
\contentsline {paragraph}{}{81}{section*.538}%
\contentsline {paragraph}{}{81}{section*.539}%
\contentsline {paragraph}{}{81}{section*.540}%
\contentsline {paragraph}{}{81}{section*.541}%
\contentsline {paragraph}{}{81}{section*.542}%
\contentsline {paragraph}{}{81}{section*.543}%
\contentsline {paragraph}{}{81}{section*.544}%
\contentsline {paragraph}{}{81}{section*.545}%
\contentsline {paragraph}{}{81}{section*.546}%
\contentsline {paragraph}{}{81}{section*.547}%
\contentsline {paragraph}{}{81}{section*.548}%
\contentsline {paragraph}{}{81}{section*.549}%
\contentsline {paragraph}{}{81}{section*.550}%
\contentsline {paragraph}{}{81}{section*.551}%
\contentsline {paragraph}{}{81}{section*.552}%
\contentsline {paragraph}{}{81}{section*.553}%
\contentsline {paragraph}{}{82}{section*.554}%
\contentsline {paragraph}{}{82}{section*.555}%
\contentsline {paragraph}{}{82}{section*.556}%
\contentsline {paragraph}{}{82}{section*.557}%
\contentsline {paragraph}{}{82}{section*.558}%
\contentsline {paragraph}{}{82}{section*.559}%
\contentsline {paragraph}{}{82}{section*.560}%
\contentsline {paragraph}{}{82}{section*.561}%
\contentsline {paragraph}{}{82}{section*.562}%
\contentsline {paragraph}{}{82}{section*.563}%
\contentsline {paragraph}{}{82}{section*.564}%
\contentsline {paragraph}{}{82}{section*.565}%
\contentsline {paragraph}{}{82}{section*.566}%
\contentsline {paragraph}{}{82}{section*.567}%
\contentsline {paragraph}{}{82}{section*.568}%
\contentsline {paragraph}{}{82}{section*.569}%
\contentsline {paragraph}{}{83}{section*.570}%
\contentsline {paragraph}{}{83}{section*.571}%
\contentsline {paragraph}{}{83}{section*.572}%
\contentsline {paragraph}{}{83}{section*.573}%
\contentsline {paragraph}{}{83}{section*.574}%
\contentsline {paragraph}{}{83}{section*.575}%
\contentsline {paragraph}{}{83}{section*.576}%
\contentsline {paragraph}{}{83}{section*.577}%
\contentsline {paragraph}{}{83}{section*.578}%
\contentsline {paragraph}{}{83}{section*.579}%
\contentsline {paragraph}{}{83}{section*.580}%
\contentsline {paragraph}{}{83}{section*.581}%
\contentsline {paragraph}{}{83}{section*.582}%
\contentsline {paragraph}{}{83}{section*.583}%
\contentsline {paragraph}{}{83}{section*.584}%
\contentsline {paragraph}{}{84}{section*.585}%
\contentsline {paragraph}{}{84}{section*.586}%
\contentsline {paragraph}{}{84}{section*.587}%
\contentsline {paragraph}{}{84}{section*.588}%
\contentsline {paragraph}{}{84}{section*.589}%
\contentsline {paragraph}{}{85}{section*.590}%
\contentsline {paragraph}{}{85}{section*.591}%
\contentsline {paragraph}{}{85}{section*.592}%
\contentsline {paragraph}{}{85}{section*.593}%
\contentsline {paragraph}{}{85}{section*.594}%
\contentsline {paragraph}{}{85}{section*.595}%
\contentsline {paragraph}{}{85}{section*.596}%
\contentsline {paragraph}{}{86}{section*.597}%
\contentsline {paragraph}{}{86}{section*.598}%
\contentsline {paragraph}{}{86}{section*.599}%
\contentsline {paragraph}{}{86}{section*.600}%
\contentsline {paragraph}{}{86}{section*.601}%
\contentsline {paragraph}{}{86}{section*.602}%
\contentsline {paragraph}{}{86}{section*.603}%
\contentsline {paragraph}{}{86}{section*.604}%
\contentsline {paragraph}{}{86}{section*.605}%
\contentsline {paragraph}{}{86}{section*.606}%
\contentsline {paragraph}{}{86}{section*.607}%
\contentsline {paragraph}{}{86}{section*.608}%
\contentsline {paragraph}{}{86}{section*.609}%
\contentsline {paragraph}{}{86}{section*.610}%
\contentsline {paragraph}{}{86}{section*.611}%
\contentsline {paragraph}{}{86}{section*.612}%
\contentsline {paragraph}{}{87}{section*.613}%
\contentsline {paragraph}{}{87}{section*.614}%
\contentsline {paragraph}{}{87}{section*.615}%
\contentsline {paragraph}{}{87}{section*.616}%
\contentsline {paragraph}{}{87}{section*.617}%
\contentsline {paragraph}{}{87}{section*.618}%
\contentsline {paragraph}{}{87}{section*.619}%
\contentsline {paragraph}{}{87}{section*.620}%
\contentsline {paragraph}{}{87}{section*.621}%
\contentsline {paragraph}{}{87}{section*.622}%
\contentsline {paragraph}{}{87}{section*.623}%
\contentsline {paragraph}{}{87}{section*.624}%
\contentsline {paragraph}{}{87}{section*.625}%
\contentsline {paragraph}{}{87}{section*.626}%
\contentsline {paragraph}{}{87}{section*.627}%
\contentsline {paragraph}{}{87}{section*.628}%
\contentsline {paragraph}{}{88}{section*.629}%
\contentsline {paragraph}{}{88}{section*.630}%
\contentsline {paragraph}{}{88}{section*.631}%
\contentsline {chapter}{\numberline {C}The JSS Dictionary}{89}{appendix.C}%
\contentsline {paragraph}{}{90}{section*.632}%
\contentsline {paragraph}{}{90}{section*.633}%
\contentsline {paragraph}{}{90}{section*.634}%
\contentsline {paragraph}{}{90}{section*.635}%
\contentsline {paragraph}{}{90}{section*.636}%
\contentsline {paragraph}{}{90}{section*.637}%
\contentsline {paragraph}{}{90}{section*.638}%
\contentsline {paragraph}{}{90}{section*.639}%
\contentsline {paragraph}{}{90}{section*.640}%
\contentsline {paragraph}{}{90}{section*.641}%
\contentsline {paragraph}{}{90}{section*.642}%
\contentsline {paragraph}{}{90}{section*.643}%
\contentsline {paragraph}{}{91}{section*.644}%
\contentsline {paragraph}{}{91}{section*.645}%
\contentsline {paragraph}{}{91}{section*.646}%
\contentsline {paragraph}{}{91}{section*.647}%
\contentsline {paragraph}{}{91}{section*.648}%
\contentsline {paragraph}{}{91}{section*.649}%
\contentsline {paragraph}{}{91}{section*.650}%
\contentsline {paragraph}{}{91}{section*.651}%
\contentsline {paragraph}{}{91}{section*.652}%
\contentsline {paragraph}{}{91}{section*.653}%
\contentsline {paragraph}{}{91}{section*.654}%
\contentsline {paragraph}{}{92}{section*.655}%
\contentsline {paragraph}{}{92}{section*.656}%
\contentsline {paragraph}{}{92}{section*.657}%
\contentsline {paragraph}{}{92}{section*.658}%
\contentsline {paragraph}{}{92}{section*.659}%
\contentsline {paragraph}{}{92}{section*.660}%
