@numchapentry{Introduction}{1}{Introduction}{1}
@numchapentry{Quick start summary}{2}{Quick start summary}{2}
@numchapentry{Loading ASDF}{3}{Loading ASDF}{3}
@numsecentry{Loading a pre-installed ASDF}{3.1}{Loading a pre-installed ASDF}{3}
@numsecentry{Checking whether ASDF is loaded}{3.2}{Checking whether ASDF is loaded}{3}
@numsecentry{Upgrading ASDF}{3.3}{Upgrading ASDF}{4}
@numsecentry{Replacing your implementation's ASDF}{3.4}{Replacing your implementation's ASDF}{4}
@numsecentry{Loading ASDF from source}{3.5}{Loading ASDF from source}{4}
@numchapentry{Configuring ASDF}{4}{Configuring ASDF}{6}
@numsecentry{Configuring ASDF to find your systems}{4.1}{Configuring ASDF to find your systems}{6}
@numsecentry{Configuring ASDF to find your systems --- old style}{4.2}{Configuring ASDF to find your systems --- old style}{7}
@numsecentry{Configuring where ASDF stores object files}{4.3}{Configuring where ASDF stores object files}{8}
@numsecentry{Resetting the ASDF configuration}{4.4}{Resetting the ASDF configuration}{9}
@numchapentry{Using ASDF}{5}{Using ASDF}{10}
@numsecentry{Loading a system}{5.1}{Loading a system}{10}
@numsecentry{Convenience Functions}{5.2}{Convenience Functions}{10}
@numsecentry{Moving on}{5.3}{Moving on}{12}
@numchapentry{Defining systems with defsystem}{6}{Defining systems with defsystem}{13}
@numsecentry{The defsystem form}{6.1}{The defsystem form}{13}
@numsecentry{A more involved example}{6.2}{A more involved example}{14}
@numsecentry{The defsystem grammar}{6.3}{The defsystem grammar}{16}
@numsubsecentry{System designators}{6.3.1}{}{18}
@numsubsecentry{Simple component names (@code {simple-component-name})}{6.3.2}{}{18}
@numsubsecentry{Complex component names}{6.3.3}{}{19}
@numsubsecentry{Component types}{6.3.4}{}{19}
@numsubsecentry{System class names}{6.3.5}{}{19}
@numsubsecentry{Defsystem depends on}{6.3.6}{}{19}
@numsubsecentry{Build-operation}{6.3.7}{}{20}
@numsubsecentry{Weakly depends on}{6.3.8}{}{20}
@numsubsecentry{Pathname specifiers}{6.3.9}{}{20}
@numsubsecentry{Version specifiers}{6.3.10}{}{21}
@numsubsecentry{Require}{6.3.11}{}{22}
@numsubsecentry{Feature dependencies}{6.3.12}{}{22}
@numsubsecentry{Using logical pathnames}{6.3.13}{}{22}
@numsubsecentry{Serial dependencies}{6.3.14}{}{23}
@numsubsecentry{Source location (@code {:pathname})}{6.3.15}{}{23}
@numsubsecentry{if-feature option}{6.3.16}{}{24}
@numsubsecentry{Entry point}{6.3.17}{}{24}
@numsubsecentry{feature requirement}{6.3.18}{}{24}
@numsecentry{Other code in .asd files}{6.4}{Other code in .asd files}{24}
@numsecentry{The package-inferred-system extension}{6.5}{The package-inferred-system extension}{25}
@numchapentry{The Object model of ASDF}{7}{The object model of ASDF}{28}
@numsecentry{Operations}{7.1}{Operations}{28}
@numsubsecentry{Predefined operations of ASDF}{7.1.1}{Predefined operations of ASDF}{30}
@numsubsecentry{Creating new operations}{7.1.2}{Creating new operations}{33}
@numsecentry{Components}{7.2}{Components}{34}
@numsubsecentry{Common attributes of components}{7.2.1}{Common attributes of components}{37}
@numsubsubsecentry{Name}{7.2.1.1}{}{37}
@numsubsubsecentry{Version identifier}{7.2.1.2}{}{37}
@numsubsubsecentry{Required features}{7.2.1.3}{}{37}
@numsubsubsecentry{Dependencies}{7.2.1.4}{}{38}
@numsubsubsecentry{pathname}{7.2.1.5}{}{39}
@numsubsubsecentry{Properties}{7.2.1.6}{}{40}
@numsubsecentry{Pre-defined subclasses of component}{7.2.2}{Pre-defined subclasses of component}{40}
@numsubsecentry{Creating new component types}{7.2.3}{Creating new component types}{41}
@numsecentry{Dependencies}{7.3}{Dependencies}{41}
@numsecentry{Functions}{7.4}{Functions}{42}
@numsecentry{Parsing system definitions}{7.5}{Parsing system definitions}{42}
@numchapentry{Controlling where ASDF searches for systems}{8}{Controlling where ASDF searches for systems}{44}
@numsecentry{Configurations}{8.1}{Configurations}{44}
@numsecentry{Truenames and other dangers}{8.2}{Truenames and other dangers}{45}
@numsecentry{XDG base directory}{8.3}{XDG base directory}{45}
@numsecentry{Backward Compatibility}{8.4}{Backward Compatibility}{45}
@numsecentry{Configuration DSL}{8.5}{Configuration DSL}{46}
@numsecentry{Configuration Directories}{8.6}{Configuration Directories}{48}
@numsubsecentry{The :here directive}{8.6.1}{The here directive}{49}
@numsecentry{Shell-friendly syntax for configuration}{8.7}{Shell-friendly syntax for configuration}{49}
@numsecentry{Search Algorithm}{8.8}{Search Algorithm}{50}
@numsecentry{Caching Results}{8.9}{Caching Results}{50}
@numsecentry{Configuration API}{8.10}{Configuration API}{51}
@numsecentry{Introspection}{8.11}{Introspection}{52}
@numsubsecentry{*source-registry-parameter* variable}{8.11.1}{*source-registry-parameter* variable}{52}
@numsubsecentry{Information about system dependencies}{8.11.2}{Information about system dependencies}{52}
@numsecentry{Status}{8.12}{Status}{52}
@numsecentry{Rejected ideas}{8.13}{Rejected ideas}{52}
@numsecentry{TODO}{8.14}{TODO}{53}
@numsecentry{Credits for the source-registry}{8.15}{Credits for the source-registry}{53}
@numchapentry{Controlling where ASDF saves compiled files}{9}{Controlling where ASDF saves compiled files}{54}
@numsecentry{Configurations}{9.1}{Output Configurations}{54}
@numsecentry{Backward Compatibility}{9.2}{Output Backward Compatibility}{55}
@numsecentry{Configuration DSL}{9.3}{Output Configuration DSL}{56}
@numsecentry{Configuration Directories}{9.4}{Output Configuration Directories}{58}
@numsecentry{Shell-friendly syntax for configuration}{9.5}{Output Shell-friendly syntax for configuration}{58}
@numsecentry{Semantics of Output Translations}{9.6}{Semantics of Output Translations}{59}
@numsecentry{Caching Results}{9.7}{Output Caching Results}{59}
@numsecentry{Output location API}{9.8}{Output location API}{59}
@numsecentry{Credits for output translations}{9.9}{Credits for output translations}{60}
@numchapentry{Error handling}{10}{Error handling}{61}
@numsecentry{ASDF errors}{10.1}{}{61}
@numsecentry{Compilation error and warning handling}{10.2}{}{61}
@numchapentry{Miscellaneous additional functionality}{11}{Miscellaneous additional functionality}{62}
@numsecentry{Controlling file compilation}{11.1}{Controlling file compilation}{62}
@numsecentry{Controlling source file character encoding}{11.2}{Controlling source file character encoding}{63}
@numsecentry{Miscellaneous Functions}{11.3}{Miscellaneous Functions}{64}
@numsecentry{Some Utility Functions}{11.4}{Some Utility Functions}{66}
@numchapentry{Getting the latest version}{12}{Getting the latest version}{70}
@numchapentry{FAQ}{13}{FAQ}{71}
@numsecentry{``Where do I report a bug?''}{13.1}{Where do I report a bug?}{71}
@numsecentry{Mailing list}{13.2}{Mailing list}{71}
@numsecentry{``What has changed between ASDF 1, ASDF 2, and ASDF 3?''}{13.3}{What has changed between ASDF 1 ASDF 2 and ASDF 3?}{71}
@numsubsecentry{What are ASDF 1, ASDF 2, and ASDF 3?}{13.3.1}{What are ASDF 1 2 3?}{71}
@numsubsecentry{How do I detect the ASDF version?}{13.3.2}{How do I detect the ASDF version?}{72}
@numsubsecentry{ASDF can portably name files in subdirectories}{13.3.3}{ASDF can portably name files in subdirectories}{72}
@numsubsecentry{Output translations}{13.3.4}{Output translations}{73}
@numsubsecentry{Source Registry Configuration}{13.3.5}{Source Registry Configuration}{73}
@numsubsecentry{Usual operations are made easier to the user}{13.3.6}{Usual operations are made easier to the user}{74}
@numsubsecentry{Many bugs have been fixed}{13.3.7}{Many bugs have been fixed}{74}
@numsubsecentry{ASDF itself is versioned}{13.3.8}{ASDF itself is versioned}{74}
@numsubsecentry{ASDF can be upgraded}{13.3.9}{ASDF can be upgraded}{74}
@numsubsecentry{Decoupled release cycle}{13.3.10}{Decoupled release cycle}{75}
@numsubsecentry{Pitfalls of the transition to ASDF 2}{13.3.11}{Pitfalls of the transition to ASDF 2}{75}
@numsubsecentry{Pitfalls of the upgrade to ASDF 3}{13.3.12}{Pitfalls of the upgrade to ASDF 3}{76}
@numsubsecentry{What happened to the bundle operations?}{13.3.13}{What happened to the bundle operations}{77}
@numsecentry{Issues with installing the proper version of ASDF}{13.4}{Issues with installing the proper version of ASDF}{78}
@numsubsecentry{``My Common Lisp implementation comes with an outdated version of ASDF. What to do?''}{13.4.1}{My Common Lisp implementation comes with an outdated version of ASDF. What to do?}{78}
@numsubsecentry{``I'm a Common Lisp implementation vendor. When and how should I upgrade ASDF?''}{13.4.2}{I'm a Common Lisp implementation vendor. When and how should I upgrade ASDF?}{78}
@numsubsecentry{After upgrading ASDF, ASDF (and Quicklisp) can't find my systems}{13.4.3}{After upgrading ASDF}{79}
@numsecentry{Issues with configuring ASDF}{13.5}{Issues with configuring ASDF}{79}
@numsubsecentry{``How can I customize where fasl files are stored?''}{13.5.1}{How can I customize where fasl files are stored?}{80}
@numsubsecentry{``How can I wholly disable the compiler output cache?''}{13.5.2}{How can I wholly disable the compiler output cache?}{80}
@numsubsecentry{How can I debug problems finding ASDF systems?}{13.5.3}{How can I debug problems finding ASDF systems}{80}
@numsecentry{Issues with using and extending ASDF to define systems}{13.6}{Issues with using and extending ASDF to define systems}{81}
@numsubsecentry{``How can I cater for unit-testing in my system?''}{13.6.1}{How can I cater for unit-testing in my system?}{81}
@numsubsecentry{``How can I cater for documentation generation in my system?''}{13.6.2}{How can I cater for documentation generation in my system?}{81}
@numsubsecentry{``How can I maintain non-Lisp (e.g. C) source files?''}{13.6.3}{How can I maintain non-Lisp (e.g. C) source files?}{81}
@numsubsecentry{``I want to put my module's files at the top level. How do I do this?''}{13.6.4}{I want to put my module's files at the top level. How do I do this?}{81}
@numsubsecentry{How do I create a system definition where all the source files have a .cl extension?}{13.6.5}{How do I create a system definition where all the source files have a .cl extension?}{82}
@numsubsecentry{How do I mark a source file to be loaded only and not compiled?}{13.6.6}{How do I mark a source file to be loaded only and not compiled?}{83}
@numsubsecentry{How do I work with readtables?}{13.6.7}{How do I work with readtables?}{83}
@numsubsubsecentry{How should my system use a readtable exported by another system?}{13.6.7.1}{}{84}
@numsubsubsecentry{How should my library make a readtable available to other systems?}{13.6.7.2}{}{84}
@numsubsecentry{How can I capture ASDF's output?}{13.6.8}{How can I capture ASDF's output?}{84}
@numsubsecentry{*LOAD-PATHNAME* and *LOAD-TRUENAME* have weird values, help!}{13.6.9}{LOAD-PATHNAME has a weird value}{85}
@numsecentry{ASDF development FAQs}{13.7}{ASDF development FAQs}{85}
@numsubsecentry{How do I run the tests interactively in a REPL?}{13.7.1}{How do I run the tests interactively in a REPL?}{85}
@unnchapentry{Ongoing Work}{10001}{Ongoing Work}{86}
@unnchapentry{Bibliography}{10002}{Bibliography}{87}
@unnchapentry{Concept Index}{10003}{Concept Index}{89}
@unnchapentry{Function and Macro Index}{10004}{Function and Macro Index}{91}
@unnchapentry{Variable Index}{10005}{Variable Index}{92}
@unnchapentry{Class and Type Index}{10006}{Class and Type Index}{93}
