\initial {B}
\entry{\code {binary-op (obsolete)}}{77}
\initial {C}
\entry{\code {compile-bundle-op}}{31, 77}
\entry{\code {compile-concatenated-source-op}}{33}
\entry{\code {compile-op}}{30}
\entry{\code {component}}{28}
\entry{\code {concatenate-source-op}}{33}
\initial {D}
\entry{\code {deliver-asd-op}}{31, 77}
\entry{\code {dll-op}}{31}
\initial {F}
\entry{\code {fasl-op (obsolete)}}{77}
\initial {I}
\entry{\code {image-op}}{31}
\initial {L}
\entry{\code {lib-op}}{31}
\entry{\code {load-bundle-op}}{31, 77}
\entry{\code {load-compiled-concatenated-source-op}}{33}
\entry{\code {load-concatenated-source-op}}{33}
\entry{\code {load-fasl-op (obsolete)}}{77}
\entry{\code {load-op}}{30}
\entry{\code {load-source-op}}{30}
\initial {M}
\entry{\code {module}}{40}
\entry{\code {monolithic-binary-op (obsolete)}}{77}
\entry{\code {monolithic-compile-bundle-op}}{31, 77}
\entry{\code {monolithic-compile-concatenated-source-op}}{33}
\entry{\code {monolithic-concatenate-source-op}}{33}
\entry{\code {monolithic-deliver-asd-op}}{31, 77}
\entry{\code {monolithic-dll-op}}{31}
\entry{\code {monolithic-fasl-op (obsolete)}}{77}
\entry{\code {monolithic-lib-op}}{31}
\entry{\code {monolithic-load-bundle-op}}{31, 77}
\entry{\code {monolithic-load-compiled-concatenated-source-op}}{33}
\entry{\code {monolithic-load-concatenated-source-op}}{33}
\entry{\code {monolithic-load-fasl-op (obsolete)}}{77}
\initial {O}
\entry{\code {operation}}{28}
\entry{\code {operation-error}}{61}
\initial {P}
\entry{\code {prepare-op}}{30}
\entry{\code {prepare-source-op}}{30}
\entry{\code {program-op}}{31}
\initial {S}
\entry{\code {source-file}}{40}
\entry{\code {system}}{40}
\entry{\code {system-definition-error}}{61}
\initial {T}
\entry{\code {test-op}}{30}
