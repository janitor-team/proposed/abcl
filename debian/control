Source: abcl
Maintainer: Debian Common Lisp Team <debian-common-lisp@lists.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Section: lisp
Priority: optional
Build-Depends: debhelper-compat (= 13),
               debhelper (>= 12.8~),
               default-jdk,
               ant,
               javahelper,
               texlive-latex-extra
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/common-lisp-team/abcl
Vcs-Git: https://salsa.debian.org/common-lisp-team/abcl.git
Homepage: https://abcl.org/
Rules-Requires-Root: no

Package: abcl
Architecture: all
Depends: ${misc:Depends},
         ${java:Depends},
         default-jre,
# See jna.patch
         libjna-java
Recommends: ${java:Recommends}
Description: Common Lisp implementation in the Java Virtual Machine
 Armed Bear Common Lisp (ABCL) is a conforming implementation of ANSI X3J13
 Common Lisp that runs in the Java Virtual Machine. It compiles Lisp code
 directly to Java byte code for execution.
 .
 It also provides the following integration methods for interfacing with Java
 code and libraries:
 .
  - Lisp code can create Java objects and call their methods;
  - Java code can call Lisp functions and generic functions, either directly
    or Java Scripting API (JSR-223);
  - it is possible to create Lisp-side implementations of
    Java interfaces that can be used as listeners for Swing classes and
    similar.
