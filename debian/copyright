Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Armed Bear Common Lisp (ABCL)
Upstream-Contact: armedbear-devel@common-lisp.net
Source: https://abcl.org/

Files: *
Copyright: 2002-2008 Peter Graves <peter@armedbear.org>
           2010-2014 Mark Evenson <evenson@panix.com>
           2008-2010 Ville Voutilainen
           2008-2012 Erik Huelsmann <ehuelsmann@common-lisp.net>
           2010 Matthias Hölzl
           2013 Nikodemus Siivola, Rudolf Schlatte
           2020 @easye
           2008-2011 Alessio Stalla
           2010 Matt Seddon
           2012-2013 Rudolf Schlatte
           2008 Hideo at Yokohama
           2002-2007 Andras Simon
           2016 Olof-Joachim Frahm
License: GPL-2+ with Classpath exception

Files: contrib/jfli/*
Copyright: Rich Hickey
License: CPL-1.0
 Common Public License Version 1.0
 .
 THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS COMMON PUBLIC
 LICENSE ("AGREEMENT"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM
 CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
 .
 1. DEFINITIONS
 .
 "Contribution" means:
 .
    a) in the case of the initial Contributor, the initial code and
 documentation distributed under this Agreement, and
 .
    b) in the case of each subsequent Contributor:
 .
    i) changes to the Program, and
 .
    ii) additions to the Program;
 .
    where such changes and/or additions to the Program originate from and are
 distributed by that particular Contributor. A Contribution 'originates' from a
 Contributor if it was added to the Program by such Contributor itself or
 anyone acting on such Contributor's behalf. Contributions do not include
 additions to the Program which: (i) are separate modules of software
 distributed in conjunction with the Program under their own license
 agreement, and (ii) are not derivative works of the Program.
 .
 "Contributor" means any person or entity that distributes the Program.
 .
 "Licensed Patents " mean patent claims licensable by a Contributor which are
 necessarily infringed by the use or sale of its Contribution alone or when
 combined with the Program.
 .
 "Program" means the Contributions distributed in accordance with this
 Agreement.
 .
 "Recipient" means anyone who receives the Program under this Agreement,
 including all Contributors.
 .
 2. GRANT OF RIGHTS
 .
    a) Subject to the terms of this Agreement, each Contributor hereby grants
 Recipient a non-exclusive, worldwide, royalty-free copyright license to
 reproduce, prepare derivative works of, publicly display, publicly perform,
 distribute and sublicense the Contribution of such Contributor, if any, and
 such derivative works, in source code and object code form.
 .
    b) Subject to the terms of this Agreement, each Contributor hereby grants
 Recipient a non-exclusive, worldwide, royalty-free patent license under
 Licensed Patents to make, use, sell, offer to sell, import and otherwise
 transfer the Contribution of such Contributor, if any, in source code and
 object code form.
 This patent license shall apply to the combination of the Contribution and
 the Program if, at the time the Contribution is added by the Contributor,
 such addition of the Contribution causes such combination to be covered by
 the Licensed Patents. The patent license shall not apply to any other
 combinations which include the Contribution. No hardware per se is licensed
 hereunder.
 .
    c) Recipient understands that although each Contributor grants the
 licenses to its Contributions set forth herein, no assurances are provided by
 any Contributor that the Program does not infringe the patent or other
 intellectual property rights of any other entity. Each Contributor disclaims
 any liability to Recipient for claims brought by any other entity based on
 infringement of intellectual property rights or otherwise. As a condition to
 exercising the rights and licenses granted hereunder, each Recipient hereby
 assumes sole responsibility to secure any other intellectual property rights
 needed, if any.  For example, if a third party patent license is required to
 allow Recipient to distribute the Program, it is Recipient's responsibility
 to acquire that license before distributing the Program.
 .
    d) Each Contributor represents that to its knowledge it has sufficient
 copyright rights in its Contribution, if any, to grant the copyright license
 set forth in this Agreement.
 .
 3. REQUIREMENTS
 .
 A Contributor may choose to distribute the Program in object code form under
 its own license agreement, provided that:
 .
    a) it complies with the terms and conditions of this Agreement; and
 .
    b) its license agreement:
 .
    i) effectively disclaims on behalf of all Contributors all warranties and
 conditions, express and implied, including warranties or conditions of title
 and non-infringement, and implied warranties or conditions of merchantability
 and fitness for a particular purpose;
 .
    ii) effectively excludes on behalf of all Contributors all liability for
 damages, including direct, indirect, special, incidental and consequential
 damages, such as lost profits;
 .
    iii) states that any provisions which differ from this Agreement are
 offered by that Contributor alone and not by any other party; and
 .
    iv) states that source code for the Program is available from such
 Contributor, and informs licensees how to obtain it in a reasonable manner on
 or through a medium customarily used for software exchange.
 .
 When the Program is made available in source code form:
 .
    a) it must be made available under this Agreement; and
 .
    b) a copy of this Agreement must be included with each copy of the
     Program.
 .
 Contributors may not remove or alter any copyright notices contained within
 the Program.
 .
 Each Contributor must identify itself as the originator of its Contribution,
 if any, in a manner that reasonably allows subsequent Recipients to identify
 the originator of the Contribution.
 .
 4. COMMERCIAL DISTRIBUTION
 .
 Commercial distributors of software may accept certain responsibilities with
 respect to end users, business partners and the like. While this license is
 intended to facilitate the commercial use of the Program, the Contributor who
 includes the Program in a commercial product offering should do so in a
 manner which does not create potential liability for other Contributors.
 Therefore, if a Contributor includes the Program in a commercial product
 offering, such Contributor ("Commercial Contributor") hereby agrees to defend
 and indemnify every other Contributor ("Indemnified Contributor") against any
 losses, damages and costs (collectively "Losses") arising from claims,
 lawsuits and other legal actions brought by a third party against the
 Indemnified Contributor to the extent caused by the acts or omissions of such
 Commercial Contributor in connection with its distribution of the Program in
 a commercial product offering. The obligations in this section do not apply
 to any claims or Losses relating to any actual or alleged intellectual
 property infringement. In order to qualify, an Indemnified Contributor must:
 a) promptly notify the Commercial Contributor in writing of such claim, and
 b) allow the Commercial Contributor to control, and cooperate with the
 Commercial Contributor in, the defense and any related settlement
 negotiations. The Indemnified Contributor may participate in any such claim
 at its own expense.
 .
 For example, a Contributor might include the Program in a commercial product
 offering, Product X. That Contributor is then a Commercial Contributor. If
 that Commercial Contributor then makes performance claims, or offers
 warranties related to Product X, those performance claims and warranties are
 such Commercial Contributor's responsibility alone. Under this section, the
 Commercial Contributor would have to defend claims against the other
 Contributors related to those performance claims and warranties, and if a
 court requires any other Contributor to pay any damages as a result, the
 Commercial Contributor must pay those damages.
 .
 5. NO WARRANTY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, THE PROGRAM IS PROVIDED ON
 AN "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER
 EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR
 CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A
 PARTICULAR PURPOSE. Each Recipient is solely responsible for determining the
 appropriateness of using and distributing the Program and assumes all risks
 associated with its exercise of rights under this Agreement, including but
 not limited to the risks and costs of program errors, compliance with
 applicable laws, damage to or loss of data, programs or equipment, and
 unavailability or interruption of operations.
 .
 6. DISCLAIMER OF LIABILITY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, NEITHER RECIPIENT NOR ANY
 CONTRIBUTORS SHALL HAVE ANY LIABILITY FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION
 LOST PROFITS), HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OR DISTRIBUTION OF THE PROGRAM OR THE
 EXERCISE OF ANY RIGHTS GRANTED HEREUNDER, EVEN IF ADVISED OF THE POSSIBILITY
 OF SUCH DAMAGES.
 .
 7. GENERAL
 .
 If any provision of this Agreement is invalid or unenforceable under
 applicable law, it shall not affect the validity or enforceability of the
 remainder of the terms of this Agreement, and without further action by the
 parties hereto, such provision shall be reformed to the minimum extent
 necessary to make such provision valid and enforceable.
 .
 If Recipient institutes patent litigation against a Contributor with respect
 to a patent applicable to software (including a cross-claim or counterclaim
 in a lawsuit), then any patent licenses granted by that Contributor to such
 Recipient under this Agreement shall terminate as of the date such litigation
 is filed. In addition, if Recipient institutes patent litigation against any
 entity (including a cross-claim or counterclaim in a lawsuit) alleging that
 the Program itself (excluding combinations of the Program with other software
 or hardware) infringes such Recipient's patent(s), then such Recipient's
 rights granted under Section 2(b) shall terminate as of the date such
 litigation is filed.
 .
 All Recipient's rights under this Agreement shall terminate if it fails to
 comply with any of the material terms or conditions of this Agreement and
 does not cure such failure in a reasonable period of time after becoming
 aware of such noncompliance. If all Recipient's rights under this Agreement
 terminate, Recipient agrees to cease use and distribution of the Program as
 soon as reasonably practicable. However, Recipient's obligations under this
 Agreement and any licenses granted by Recipient relating to the Program shall
 continue and survive.
 .
 Everyone is permitted to copy and distribute copies of this Agreement, but in
 order to avoid inconsistency the Agreement is copyrighted and may only be
 modified in the following manner. The Agreement Steward reserves the right to
 publish new versions (including revisions) of this Agreement from time to
 time.  No one other than the Agreement Steward has the right to modify this
 Agreement.  IBM is the initial Agreement Steward. IBM may assign the
 responsibility to serve as the Agreement Steward to a suitable separate
 entity. Each new version of the Agreement will be given a distinguishing
 version number. The Program (including Contributions) may always be
 distributed subject to the version of the Agreement under which it was
 received. In addition, after a new version of the Agreement is published,
 Contributor may elect to distribute the Program (including its Contributions)
 under the new version. Except as expressly stated in Sections 2(a) and 2(b)
 above, Recipient receives no rights or licenses to the intellectual property
 of any Contributor under this Agreement, whether expressly, by implication,
 estoppel or otherwise. All rights in the Program not expressly granted under
 this Agreement are reserved.
 .
 This Agreement is governed by the laws of the State of New York and the
 intellectual property laws of the United States of America. No party to this
 Agreement will bring a legal action under this Agreement more than one year
 after the cause of action arose. Each party waives its rights to a jury trial
 in any resulting litigation.

Files: contrib/jss/*
Copyright: 2005 Alan Ruttenberg
           2011-2 Mark Evenson
License: Zlib
 This software is provided 'as-is', without any express or
 implied warranty.
 .
 In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it
 and redistribute it freely, subject to the following
 restrictions:
 .
 1. The origin of this software must not be misrepresented; you
    must not claim that you wrote the original software. If you
    use this software in a product, an acknowledgment in the
    product documentation would be appreciated but is not
    required.
 .
 2. Altered source versions must be plainly marked as such, and
    must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.

Files: contrib/named-readtables/*
Copyright: 2007 - 2009 Tobias C. Rittweiler <tcr@freebits.de>
           2007, Robert P. Goldman <rpgoldman@sift.info> and SIFT, LLC
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 .
     * Neither the names of Tobias C. Rittweiler, Robert P. Goldman,
       SIFT, LLC nor the names of its contributors may be used to
       endorse or promote products derived from this software without
       specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY Tobias C. Rittweiler, Robert
 P. Goldman and SIFT, LLC ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Tobias C. Rittweiler, Robert
 P. Goldman or SIFT, LLC BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: contrib/named-readtables/test/rt.lisp
       test/lisp/abcl/rt.lisp
Copyright: 1990, the Massachusetts Institute of Technology, Cambridge MA
License: NTP~disclaimer
 Permission  to  use,  copy, modify, and distribute this software  and  its
 documentation for any purpose  and without fee is hereby granted, provided
 that this copyright  and  permission  notice  appear  in  all  copies  and
 supporting  documentation,  and  that  the  name  of M.I.T. not be used in
 advertising or  publicity  pertaining  to  distribution  of  the  software
 without   specific,   written   prior   permission.      M.I.T.  makes  no
 representations  about  the  suitability of this software for any purpose.
 It is provided "as is" without express or implied warranty.
 .
  M.I.T. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,  INCLUDING
  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
  M.I.T. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL  DAMAGES  OR
  ANY  DAMAGES  WHATSOEVER  RESULTING  FROM  LOSS OF USE, DATA OR PROFITS,
  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER  TORTIOUS  ACTION,
  ARISING  OUT  OF  OR  IN  CONNECTION WITH THE USE OR PERFORMANCE OF THIS
  SOFTWARE.

Files: doc/asdf/asdf.texinfo
       src/org/armedbear/lisp/asdf.lisp
Copyright: 2001-2019 Daniel Barlow, Francois-Rene Rideau, Robert P. Goldman, and contributors
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/org/armedbear/lisp/loop.lisp
Copyright: 2004-2007 Peter Graves
           1986, the Massachusetts Institute
           1989, 1990, 1991, 1992 by Symbolics, Inc
License: GPL-2+ with Classpath exception, and NTP

Files: examples/java-exception/*
       examples/java-interface/*
       examples/java-to-lisp-1/*
       examples/java-to-lisp-2/*
       examples/lisp-to-java/*
       examples/misc/update-check-enabled.lisp
       examples/pure-lisp-to-java/*
       examples/gui/*
Copyright: 2008 Ville Voutilainen
           2003-2006 Peter Graves
License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+ with Classpath exception
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.
 .
 A section numbered 13 has been added to the text of the GPL, version 2:
 .
     13. Linking this library statically or dynamically with other
     modules is making a combined work based on this library. Thus, the
     terms and conditions of the GNU General Public License cover the
     whole combination.
 .
     The following paragraph details the "classpath exception" which ABCL
     allows as an exception to the statement about linking libraries.
 .
     As a special exception, the copyright holders of this software give
     you permission to link this software with independent modules to
     produce an executable, regardless of the license terms of these
     independent modules, and to copy and distribute the resulting
     executable under terms of your choice, provided that you also meet,
     for each linked independent module, the terms and conditions of the
     license of that module. An independent module is a module which is
     not derived from or based on this software. If you modify this
     software, you may extend this exception to your version of the
     software, but you are not obligated to do so. If you do not wish to
     do so, delete this exception statement from your version.

License: NTP
 Permission to use, copy, modify and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the M.I.T. copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation. The names "M.I.T." and "Massachusetts
 Institute of Technology" may not be used in advertising or publicity
 pertaining to distribution of the software without specific, written
 prior permission. Notice must be given in supporting documentation that
 copying distribution is by permission of M.I.T. M.I.T. makes no
 representations about the suitability of this software for any purpose.
 It is provided "as is" without express or implied warranty.
 .
 Permission to use, copy, modify and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the Symbolics copyright notice appear in all copies and
 that both that copyright notice and this permission notice appear in
 supporting documentation. The name "Symbolics" may not be used in
 advertising or publicity pertaining to distribution of the software
 without specific, written prior permission. Notice must be given in
 supporting documentation that copying distribution is by permission of
 Symbolics. Symbolics makes no representations about the suitability of
 this software for any purpose. It is provided "as is" without express
 or implied warranty.
 .
 Symbolics, CLOE Runtime, and Minima are trademarks, and CLOE, Genera,
 and Zetalisp are registered trademarks of Symbolics, Inc.
 .
      Symbolics, Inc.
      8 New England Executive Park, East
      Burlington, Massachusetts  01803
      United States of America
      +1-617-221-1000
