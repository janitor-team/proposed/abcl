;; Execute ABCL’s own testsuite
;; This is the equivalent of “ant test.abcl”

;; Note that we don’t run the ANSI testsuite and the cl-bench testsuite,
;; because they require to download external source code.
;; See “ant help.test” for all testsuites.

(require "asdf")

;; Add current directory to ASDF registry, so that abcl.asd can be found
(asdf:initialize-source-registry `(:source-registry (:directory ,*default-pathname-defaults*) :inherit-configuration))

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "abcl/test/lisp"))

(in-package :abcl.test.lisp)

;; Exclude some tests that currently fail (this needs to be clarified with upstream)
(setq *expected-failures* (append *expected-failures*
                                  '(unused.1
                                    derive-type-logxor.2 derive-type-logxor.3
                                    with-code-to-method.2 directory.3
                                    physical.21 physical.27
                                    lots-of-dots.1 lots-of-dots.2
                                    silly.5 jar-pathname.truename.1
                                    expt.5 math.read-from-string.1
                                    bugs.pprint.1 bugs.reader.1
                                    serialization-of-closure
                                    compiler.3 dmc-test-args-with-optional.1a 
                                    dmc-test-args-with-optional.1b dmc-test-args-with-optional.2a 
                                    dmc-test-args-with-optional.2b dmc-test-args-with-optional.3 
                                    dmc-test-args-with-optional.4a dmc-test-args-with-optional.4b 
                                    dmc-test-args-with-optional.4c math.max.1 funcall.1 funcall.2 
                                    funcall.3 funcall.4 funcall.5 destructuring-bind.3 bugs.java.1 
                                    bugs.clos.aux.1 bugs.export.1)))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless.
;; NB: the “run” function returns success iff there is no failure (i.e. it
;; fails even if there are only expected failures)
(unless (or (run)
            (null (set-difference *failed-tests* *expected-failures*)))
  (uiop:quit 1))
